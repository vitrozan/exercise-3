from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import WebDriverException
from django.test import LiveServerTestCase
import time
import unittest

MAX_WAIT = 10

class NewVisitorTest(LiveServerTestCase):

	def setUp(self):
		self.browser = webdriver.Chrome()
	
	#def test_can_start_a_list_and_retrieve_it_later(self):
		#self.browser.get(self.live_server_url)
		#self.assertIn('Tugas PKMPL Tioooooo To-Do',self.browser.title)

	def tearDown(self):
		self.browser.quit()
		super(NewVisitorTest, self).tearDown()
					
	
	def test_input_a_todo_then_comment_change(self):
        # cek browsernya
		self.browser.get(self.live_server_url)
		
		
		
        # Cek komentar
		self.check_for_comment("yey, waktunya berlibur")
        # Cek komentar
		self.input_something_to_list(1, 'Hayo lo tugas pertama')
        # Cek komentar
		self.check_for_comment("sibuk tapi santai")
		self.input_something_to_list(2, 'Bentar lagi uts selesai')
		self.input_something_to_list(3, 'Rencana bukber eh ada covid')
		self.input_something_to_list(4, 'Ok')
		self.input_something_to_list(5, 'DUARRRRRRR yaudah lah ya')
        # Cek komentar
		self.check_for_comment("oh tidak")



	def check_for_comment(self, teks):
		comment = self.browser.find_element_by_id('comment')
		self.assertEqual(teks, comment.text)

	def check_for_row_in_list_table(self, row_text):
		table = self.browser.find_element_by_id('id_list_table')
		rows = table.find_elements_by_tag_name('tr')
		self.assertIn(row_text, [row.text for row in rows])  
    
	def input_something_to_list(self, number, text):
		inputbox = self.browser.find_element_by_id('id_new_item')
		inputbox.send_keys(text)
        
		inputbox.send_keys(Keys.ENTER)
		time.sleep(1)
		self.check_for_row_in_list_table(str(number) + ': ' + text)

	def  wait_for_row_in_list_table(self,row_text):
		start_time = time.time()
		while True:
			try:
				table = self.browser.find_element_by_id('id_list_table')
				rows = table.find_elements_by_tag_name('tr')
				self.assertIn(row_text,[row.text for row in rows])
				return
			except (AssertionError,WebDriverException) as e:
				if time.time() - start_time > MAX_WAIT:
					raise e
				time.sleep(0.5)
	

if __name__ == '__main__':
    unittest.main(warnings='ignore')